
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', redirect: '/train' },
      // { path: '', component: () => import('pages/Index.vue') },
      { path: '/train', name: 'Treinamento da prótese', component: () => import('pages/Train.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes

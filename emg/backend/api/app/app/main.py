from typing import List
import time
from fastapi import FastAPI, WebSocket, WebSocketDisconnect
from fastapi.responses import HTMLResponse
import random
import asyncio

app = FastAPI()


def get_rand(data, labels):
    if not data:
        labels = ['{} s'.format(nbr/10) for nbr in range(150)]
        data =[random.randint(0, 60) for idx in range(150)]
        return data, labels
    data.append(random.randint(0, 60))
    last = float(labels[-1][:-2])
    labels.append('{:.1f} s'.format(last + 0.1))
    return data[1:], labels[1:]

class ConnectionManager:
    def __init__(self):
        self.active_connections: List[WebSocket] = []

    async def connect(self, websocket: WebSocket):
        await websocket.accept()
        self.active_connections.append(websocket)

    def disconnect(self, websocket: WebSocket):
        self.active_connections.remove(websocket)

    async def send_personal_message(self, message: str, websocket: WebSocket):
        await websocket.send_json(message)

    async def broadcast(self, message: str):
        for connection in self.active_connections:
            await connection.send_text(message)


manager = ConnectionManager()


@app.websocket("/ws/{client_id}")
async def websocket_endpoint(websocket: WebSocket, client_id: int):
    await manager.connect(websocket)
    data = []
    labels = []
    try:
        while True:
            await asyncio.sleep(0.1)
            data, labels = get_rand(data, labels)
            mdata = {
                "labels": labels,
                "datasets": [{
                  "label": "Sinal eletromiográfico",
                  "data": data,
                  "pointRadius": 0.1,
                  "pointStyle": "circle"
                }]
            }
            await manager.send_personal_message(mdata, websocket)
    except WebSocketDisconnect:
        manager.disconnect(websocket)
        await manager.broadcast(f"Cliente #{client_id} desconectou o socket")
